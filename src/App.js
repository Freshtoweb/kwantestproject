import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// Import Teacher and Student
import Teacher from './Classes/Person/Teacher/Main';
import Student from './Classes/Person/Student/Main';


class App extends Component {
  constructor(props){
      super(props);

      this.state = {
          semester: [],
          teacher: this.buildClass()
      };
  }
  buildClass(){
      var studentNames = [
          {
              firstName: "Billy",
              lastName: "Joel"
          },{
              firstName: "Michael",
              lastName: "Jackson"
          },{
              firstName: "Billy",
              lastName: "Joel"
          },
          {
              firstName: "Michael",
              lastName: "Jackson"
          },{
              firstName: "Billy",
              lastName: "Joel"
          },
          {
              firstName: "Michael",
              lastName: "Jackson"
          },{
              firstName: "Billy",
              lastName: "Joel"
          },
          {
              firstName: "Michael",
              lastName: "Jackson"
          }
      ];

      var myClass = studentNames.map(function(studentName){
          return new Student(studentName);
      });

      return new Teacher(
          {
              firstName: "Mrs",
              lastName: "Puff"
          },
          myClass
      );
  }
  simulateSemester(){
      this.setState({
         semester: this.state.teacher.getSemester(),
         teacher: this.buildClass()
      });
  }
  render() {
      var myTeacher = this.state.teacher;
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="content-area">
            <div className="class-room">
                <div className="teacher">
                    <h1>{myTeacher.name.firstName} {myTeacher.name.lastName}</h1>
                </div>
                <div className="students">
                    {this.state.semester.map(function(studentObj){
                        return (
                            <div className="student">
                                <div className="student-img">
                                    <img src="https://randomuser.me/api/portraits/men/10.jpg" />
                                </div>
                                <div className="student-name">
                            <span>
                                <b>
                                    {studentObj.name.firstName} {studentObj.name.lastName}
                                </b>
                            </span>
                                </div>
                                <div className="grade-value">
                            <span>
                                <b>Final Grade:</b> {parseInt(studentObj.finalGrade * 100)}
                            </span>
                                </div>

                            </div>
                        );
                    })}
                </div>
            </div>
            <button onClick={() => this.simulateSemester()}>Simulate Semester</button>
        </div>
      </div>
    );
  }
}

export default App;
