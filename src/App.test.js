import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

// ----------------------------------------------------
//                    Student Tests
// ----------------------------------------------------

import Student from './Classes/Person/Student/Main';

var student = new Student({
  firstName: "Billy",
  lastName: "Bob"
});

it('Add Grades to list', () => {
  // Test one
  student.setGrade(100);
  expect(student.gradeList.length).toEqual(1);

  // Test two
  student.setGrade(50);
  expect(student.gradeList.length).toEqual(2);
});

it('Get Total Grade', () => {
  // Test one
  expect(student.getTotalGrade()).toEqual(150);

  // Test two
  student.setGrade(100);
  expect(student.getTotalGrade()).toEqual(250);
});

it('Student selects an answer', () => {
  var question = {"answerList":[{"isCorrect":true},{"isCorrect":false},{"isCorrect":false}]};

  var studentAnswer = student.answerQuestion(question);

  expect( typeof(studentAnswer) ).toEqual("number");
});

// ----------------------------------------------------
//                    Student Tests[END]
// ----------------------------------------------------