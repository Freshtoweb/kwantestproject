export default class Person{
    // Person base class
    constructor(name) {
        this.name = {
            ...name
        };
    }
}