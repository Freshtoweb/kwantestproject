import Person from '../Main';

export default class Student extends Person{
    constructor(name){
        // Extend person class and push props to super constructor
        super(name);
        this.gradeList = [];

    }

    // Set individual quiz grade
    setGrade(grade){
        this.gradeList.push(grade);
    }

    // Student Takes quiz
    takeQuiz(quiz) {
        return quiz.questions.map((question) => this.answerQuestion(question));
    }

    // Student answers questions inside quiz
    answerQuestion(question){
        return Math.floor(Math.random() * (question.answerList.length -  1));
    }

    // Returns the students total grade
    getTotalGrade(){
        return this.gradeList.reduce((sum, grade) => sum + grade);
    }
}