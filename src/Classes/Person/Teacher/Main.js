import Person from '../Main';
import Quiz from '../../Quiz/Main';

export default class Teacher extends Person{
    constructor(name, myClass){
        super(name);
        this.myClass = myClass;
        
    }
    
    // Issue quiz to teacher myClass property
    issueQuiz(){
        var quiz = this.generateQuiz();
        this.myClass.map((student) => {
            var studentAnswers = student.takeQuiz(quiz);
            student.setGrade(this.gradeQuiz(quiz.questions, studentAnswers));
        });
    }
    
    // Grade quizzes that myClass property answer
    gradeQuiz(quizAnswerKey, studentAnswers){
        var correctAnswers = 0;

        for(var i = 0; i < quizAnswerKey.length; i++){
            if(quizAnswerKey[i].getCorrectAnswer() === studentAnswers[i]){
                correctAnswers++;
            }
        }

        return correctAnswers / ( quizAnswerKey.length );
    }
    
    // Generates a quiz for myClass to take
    generateQuiz(){
        var questionList = [];
        var numberOfQuestions = Math.floor(Math.random() * 10) + 2;
        for(var i = 0; i < numberOfQuestions; i++){
            questionList.push(this.generateQuestion());
        }

        return new Quiz(questionList);
    }
    
    // Generates a question for quiz
    generateQuestion(){
        var question = [];

        var numberOfAnswers = Math.floor(Math.random() * 2) + 2;

        var chosenAnswer = Math.floor(Math.random() * numberOfAnswers) + 1;

        for(var i = 1; i <= numberOfAnswers; i++){
            question.push({
                // Todo implement string answer
                isCorrect: (i == chosenAnswer)
            });
        }

        return question;
    }
    
    // Simulates an entire semester worth of quizzes
    getSemester(){
        this.semesterWeeks = Math.floor(Math.random() * 2) + 3;
        for(var i = 0; i < this.semesterWeeks; i++){
            this.issueQuiz();
        }

        return this.getFinalGrades();
    }
    
    // Calculate myClass grade props
    getFinalGrades(){
        return this.myClass.map((student) => {
            return {
                ...student,
                finalGrade: student.getTotalGrade() / this.semesterWeeks
            };
        });
    }
}