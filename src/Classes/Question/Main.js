export default class Question{
    constructor(answerList){
        this.answerList = answerList;
    }
    getCorrectAnswer(){
        // Returns correct answer for question
        return this.answerList.findIndex((answer) => {
            if(answer.isCorrect === true){
                return true;
            }
        });
    }
}