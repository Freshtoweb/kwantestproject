// Quiz general class 
import Question from '../Question/Main';
export default class Quiz{
    constructor(questions){
        // Maps question objects from questions array
        this.questions = questions.map((questionObj) => new Question(questionObj));
    }
}